<?php

return array(
    /*
	|--------------------------------------------------------------------------
	| Client ID
	|--------------------------------------------------------------------------
	|
	| Your Instagram client_id.
	|
	*/
	'client_id' 	=> 	'9c7a826972cc40efa1884d36a4f6b3b0',

	/*
	|--------------------------------------------------------------------------
	| Client Secret
	|--------------------------------------------------------------------------
	|
	| Your Instagram client_secret.
	|
	*/
	'client_secret'	=>	'c2e45123acc04031a8ab4de41aae7313',

	/*
	|--------------------------------------------------------------------------
	| Redirect URI
	|--------------------------------------------------------------------------
	|
	| The redirect_uri you used in the authorization request.
	| Note: this has to be the same value as in the authorization request.
	|
	*/
	'redirect_uri'	=>	'https://listerine.co/yo-acepto-el-reto-en-instagram/instagram/authorized',

	/*
	|--------------------------------------------------------------------------
	| Access Token
	|--------------------------------------------------------------------------
	|
	| Since we need to access instagram's public API without a user session
	| we save an access token here for successful query
	|
	*/
	'default_access_token' => '188488451.da846ba.5b9f8ae631704da5bdfbaa2d42b13a09',

	/*
	|--------------------------------------------------------------------------
	| Scope
	|--------------------------------------------------------------------------
	|
	| Allows you to specify the scope of the access you’re requesting from the user.
	| Currently, all apps have basic read access by default.
	|
	| Here are the scopes we currently support:
	|	* basic - to read any and all data related to a user (granted by default)
	|	* comments - to create or delete comments on a user’s behalf
	|	* relationships - to follow and unfollow users on a user’s behalf
	|	* likes - to like and unlike items on a user’s behalf
	|
	*/
	'scope'			=> array('basic'),

	/*
	|--------------------------------------------------------------------------
	| Session name
	|--------------------------------------------------------------------------
	|
	| Referencing session key to store access token.
	|
	*/
	'session_name' 	=> 'instagram_access_token',

	/*
	|--------------------------------------------------------------------------
	| Target Hashtags
	|--------------------------------------------------------------------------
	|
	| list all the hashtags this app will look for in the users feed
	|
	*/
	'hashtags' => array('YoAceptoElReto')
);