<?php
$instagram = App::make('Listerine\Instagram\Contracts\InstagramClient');
$photosRepo = App::make('Listerine\Repositories\Contracts\PhotosProvider');

Event::subscribe(new InstagramMediaEventHandler($instagram, $photosRepo));