<?php

use Listerine\Repositories\Contracts\PhotosProvider;
use Listerine\Instagram\Contracts\InstagramClient;

class InstagramMediaEventHandler {

    /**
     * instagram collector
     * @var [type]
     */
    protected $instagram;

    /**
     * photos provider implementation
     * @var [type]
     */
    protected $photos;
    //----------------------------------------------------------

    /**
     * constructor
     * @param InstagramClient $instagram
     * @param PhotosProvider  $photos
     */
	public function __construct(InstagramClient $instagram, PhotosProvider $photos)
	{
		$this->instagram = $instagram;
        $this->photos = $photos;
	}

	/**
     * Handle user login events.
     */
    public function onNewTagMedia($media)
    {
        foreach($media as $photo) {
        	$owner            = $photo->getUser();
            $followsListerine = $this->instagram->userFollowsAccount($owner);

            if($followsListerine) {
                $this->photos->firstOrCreate($photo, $owner);
            }
        }
    }

    /**
     * Handle user logout events.
     */
    public function onUserNewMedia($instagramUser)
    {
    	$follows = $this->instagram->getFollows($instagramUser);
    	$followsListerine = $this->instagram->userFollowsAccount($instagramUser);

    	if($followsListerine) {
    		//get user media with hashtag
    		$media = $this->instagram->getMediaFromUser($instagramUser);

            //create media
            foreach($media as $photo) {
                $this->photos->firstOrCreate($photo, $instagramUser);
            }
    	}
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     * @return array
     */
    public function subscribe($events)
    {
        $events->listen('tag.media.new', 'InstagramMediaEventHandler@onNewTagMedia');

        $events->listen('user.media.new', 'InstagramMediaEventHandler@onUserNewMedia');
    }
}