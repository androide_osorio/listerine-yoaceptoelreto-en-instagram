<?php

use \Carbon\Carbon;

class Photo extends Eloquent {

	/**
	 * guarded attributes from mass assignment
	 */
	public $guarded = array();

	//------------------------------------
	/**
	 * scope for winner posts
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeWinners($query)
	{
		return $query->where('has_won', 1);
	}

	/**
	 * scope all visible photos
	 * @return QueryBuilder
	 */
	public function scopeVisible($query)
	{
		return $query->where('visible', 1);
	}

	//------------------------------------

	/**
	 * returns true if the photo was published today
	 * @return boolean
	 */
	public function publishedToday()
	{
		return $this->created_at->isToday();
	}

	/**
	 * determines if the instagram photo is missing
	 * @return boolean
	 */
	public function isMissing()
	{
		$headers = get_headers($this->image_url);

		return strpos($headers[0],'404') !== false;
	}

	/**
	 * convert date fields to carbon objects
	 * @return array
	 */
	public function getDates()
	{
		return array('created_at', 'updated_at', 'has_won_at');
	}
}
