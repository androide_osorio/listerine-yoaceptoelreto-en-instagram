<?php

use \Instagram;

class InstagramController extends BaseController {

	/**
	 * saves the instagram session name
	 * @var string
	 */
	private $instagram_session_name;

	/**
	 * true if this session has an instagram auth
	 * @var boolean
	 */
	private $has_session;

	/**
	 * constructor
	 * sets up instance variables and config variables
	 */
	public function __construct()
	{
		$this->instagram_session_name = Config::get('instagram::session_name');
		$this->has_session = Session::has($this->instagram_session_name);
	}

	//---------------------------------------------------------

	/**
	 * callback URI
	 * this action sets up the instagram session after successful authorization
	 *
	 * @return Illuminate\Http\Response
	 */
	public function getAuthorize()
	{
		if ($this->has_session) {
			Session::forget($this->instagram_session_name);
		}

		$oauth_code   = Input::get('code');
		$access_token = Instagram::getAccessToken($oauth_code);

		Session::put($this->instagram_session_name, $access_token);

    return View::make('instagram.authorized');
	}

	/**
	 * Login action
	 * Redirects to instagram login and authorization url
	 *
	 * @return Illuminate\Http\Response
	 */
	public function getLogin()
	{
		if ($this->has_session) {
			Session::forget($this->instagram_session_name);
		}

    Instagram::authorize();
	}

	/**
	 * Logout action
	 *
	 * logs the user out of instagram, revoking authorization
	 *
	 * @return Illuminate\Http\Response
	 */
	public function getLogout()
	{
		Session::forget($this->instagram_session_name);

    return Redirect::to('/');
	}

	/**
	 * renders authorization window content
	 * @return [type] [description]
	 */
	public function renderAuthorize()
	{
		return View::make('instagram.authorize');
	}


	/**
	 * checks for n istagram session and returns the currently logged in user
	 * @return [type] [description]
	 */
	protected function getInstagramUser()
	{
		$instagram_session_name = Config::get('instagram::session_name');
		$user = null;

		if(Session::has($instagram_session_name)) {
			$user = Instagram::getCurrentUser();
		}

		return $user;
	}

}
