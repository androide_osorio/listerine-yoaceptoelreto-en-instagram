<?php

use Listerine\Instagram\Contracts\InstagramClient;

class PagesController extends BaseController {

	/**
	 * renders terms and conditions page
	 * @return [type] [description]
	 */
	public function renderTerms()
	{
		return View::make('legals.terms-and-conditions');
	}

	/**
	 * render the how to participate page
	 * @return [type] [description]
	 */
	public function renderHowTo()
	{
		return View::make('howto');
	}

}
