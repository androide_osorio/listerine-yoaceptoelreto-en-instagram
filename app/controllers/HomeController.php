<?php

use Carbon\Carbon;
use Listerine\Instagram\Contracts\InstagramClient;
use Listerine\Repositories\Contracts\PhotosProvider;


/*
|--------------------------------------------------------------------------
| Default Home Controller
|--------------------------------------------------------------------------
|
| You may wish to use controllers instead of, or in addition to, Closure
| based routes. That's great! Here is an example controller method to
| get you started. To route to this controller, just add the route:
|
|	Route::get('/', 'HomeController@showWelcome');
|
*/
class HomeController extends BaseController {

	/**
	 * instagram collector instance
	 * @var InstagramCollector
	 */
	private $instagram;

	/**
	 * photos repository
	 * @var PhotosProvider
	 */
	private $photos;

	/**
	 * a date marking the beginning of the contest
	 * @var Carbon
	 */
	private $starts_at;

	/**
	 * a date marking the end of the contest
	 * @var Carbon
	 */
	private $ends_at;

	//----------------------------------------------------------

	/**
	 * constructor.
	 * uses depency Injection
	 *
	 * @param InstagramClient $instagram
	 * @param PhotosProvider  $repo
	 */
	public function __construct(InstagramClient $instagram, PhotosProvider $repo)
	{
		$this->instagram = $instagram;
		$this->photos = $repo;

		$this->starts_at = Carbon::createFromDate(2015, 06, 05, "America/Bogota");
		$this->ends_at = Carbon::createFromDate(2015, 07, 05, "America/Bogota");
	}

	/**
	 * render the main application view
	 * @return Response
	 */
	public function index()
	{
		$tag     = $this->instagram->getHashtag();
		$user    = $this->getInstagramUser();

		$ends_at = $this->ends_at->isFuture() ? Carbon::tomorrow() : $this->ends_at;
		$visibleMedia = $this->photos->publishedBetween($this->starts_at, $ends_at);

		$winners = $this->photos->winners();

		if($user) {
			Event::fire('user.media.new', array($user));
		}

		return View::make('index')
			->with('media', $visibleMedia)
			->with('hashtag', $tag)
			->with('all_winners', $winners);
	}

	/**
	 * checks for n istagram session and returns the currently logged in user
	 * @return [type] [description]
	 */
	protected function getInstagramUser()
	{
		$instagram_session_name = Config::get('instagram::session_name');
		$user = null;

		if(Session::has($instagram_session_name)) {
			$user = Instagram::getCurrentUser();
		}

		return $user;
	}

}
