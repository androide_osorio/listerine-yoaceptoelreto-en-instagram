<?php

use Listerine\Repositories\Contracts\PhotosProvider;
use Listerine\Instagram\Contracts\InstagramClient;

class AdminController extends BaseController {

	/**
	 * photos provider implementation
	 * @var [type]
	 */
	protected $photos;

	//----------------------------------------------------------
	/**
	 * constructor
	 * uses dependency injection
	 *
	 * @param PhotosProvider $repository
	 */
	public function __construct(InstagramClient $instagram, PhotosProvider $repository)
	{
		$this->instagram = $instagram;
		$this->photos = $repository;
	}

	/**
	 * render index admin view
	 * @return Response
	 */
	public function index()
	{
		$media       = $this->instagram->mediaWithHashtag();
		$todaysMedia = $this->photos->recent();
		$winners     = $this->photos->winners();
		$winnerUsers = $this->photos->winnerUsers();

		Event::fire('tag.media.new', array($media));

		return View::make('admin.index')
			->with('media', $todaysMedia)
			->with('all_winners', $winners)
			->with('winner_users', $winnerUsers);
	}

	/**
	 * POST endpoint for storing winners selected in the admin
	 * @return Response
	 */
	public function store()
	{
		$photos = Input::get('photos');
		$message = '';

		if($this->photos->updateMany($photos)) {
			$message .= 'todas las fotos elegidas han sido publicadas.';
			$message .= 'todas las fotos elegidas han sido marcadas como ganadoras.';
		}

		return Redirect::back()->with('message', $message);
	}

	public function monitorize()
	{
		$media = $this->instagram->mediaWithHashtag();
		dd($media);
	}
}
