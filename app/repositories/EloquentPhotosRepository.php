<?php namespace Listerine\Repositories;

use \DB;
use \Photo;
use Carbon\Carbon;
use Listerine\Repositories\Contracts\PhotosProvider;

class EloquentPhotosRepository implements PhotosProvider {

	/**
	 * reference to the model
	 * @var Photo
	 */
	protected $Photo;

	//----------------------------------------------------------

	/**
	 * constructor
	 * @param Photo $model [description]
	 */
	public function __construct(Photo $model)
	{
		$this->Photo = $model;
	}

	/**
	 * retrieves all photos
	 * @return Collection
	 */
	public function all()
	{
		return $this->Photo->all();
	}

	/**
	 * return all the photos ordered by creation date (from earliest to oldest)
	 * @return Collection
	 */
	public function recent()
	{
		return $this->Photo->orderBy('created_at', 'DESC')->get();
	}

	/**
	 * creates a new media or retrieves the media object
	 * if it was already in database
	 *
	 * @param $mediaInfo
	 * @param $owner
	 *
	 * @return Photo
	 */
	public function firstOrCreate($mediaInfo, $owner)
	{
		//get user media with hashtag
    $mediaData = array(
        'username'      => $owner->getUserName(),
        'id'            => $mediaInfo->getId(),
        'image_url'     => $mediaInfo->getStandardResImage()->url,
        'instagram_url' => $mediaInfo->getLink(),
    );

    $photo = $this->Photo->firstOrCreate($mediaData);

    $photo->created_at = $mediaInfo->getCreatedTime('Y-m-d H:i:s');
    $photo->save();

    return $photo;
	}

	/**
	 * return all winner photos organized by date
	 * @return Collection
	 */
	public function winners()
	{
		$winners = $this->Photo->winners()->orderBy('has_won_at', 'DESC')->get();

		return $winners->groupBy(function($media)
		{
			return $media->has_won_at->formatLocalized('%d %B %Y');
		});
	}

	/**
	 * return all the users that have won with any photo
	 * @return array
	 */
	public function winnerUsers()
	{
		$allUsers = $this->Photo->winners()->get()->lists('username');

		return array_unique($allUsers);
	}

	/**
	 * return all the photos that have been shared today
	 * @return Collection
	 */
	public function publishedToday()
	{
		$today    = Carbon::createFromDate(2015, 06, 05, "America/Bogota");
		$tomorrow = Carbon::tomorrow()->toDateString();

		return $this->Photo->visible()
			->whereBetween(
				DB::raw('date(created_at)'), array($today, $tomorrow)
			)
			->orderBy('created_at', 'DESC')
			->get();
	}

	/**
	 * return only visible photos publiushed between the date range
	 *
	 * @param  Carbon $date1
	 * @param  Carbon $date2
	 * @return Collection
	 */
	public function publishedBetween($date1, $date2)
	{

		return $this->Photo->visible()
			->whereBetween(
				DB::raw('date(created_at)'), array($date1, $date2)
			)
		  ->orderBy('created_at', 'DESC')
			->get();
	}

	/**
	 * return only the specified photos
	 * @param  array $ids
	 * @return Collection
	 */
	public function only(array $ids)
	{
		return $this->Photo->whereIn('id', $ids)->get();
	}

	/**
	 * get all photos except those listed
	 * @param  array  $ids
	 * @return Collection
	 */
	public function except(array $ids)
	{
		return $this->Photo->whereNotIn('id', $ids)->get();
	}

	/**
	 * return only the visible images
	 * @return Collection
	 */
	public function visible()
	{
		return $this->Photo->where('visible', 1);
	}

	/**
	 * updates a single photo
	 * @param  string $photo_id the photo id
	 * @param  array $fields    an array with the updated fields
	 * @return boolean
	 */
	public function update($photo_id, array $fields)
	{
		$photo = $this->Photo->find($photo_id);

		if(!array_key_exists('visible', $fields)) {
			$fields['visible'] = 0;
		}
		if(!array_key_exists('has_won', $fields)) {
			$fields['has_won'] = 0;
		}

		return $photo->update($fields);
	}

	/**
	 * updates many photos at once
	 * @param  array  $photos photos array with field info
	 * @return boolean
	 */
	public function updateMany(array $photos)
	{
		foreach($photos as $photo_id => $fields) {
			$fields = array_filter($fields);

			$this->update($photo_id, $fields);
		}

		return true;
	}

	/**
	 * make the desired photo visible
	 * @param  string $id
	 * @return boolean
	 */
	public function publish($id)
	{
		$photo = $this->Photo->findOrFail($id);

		$photo->visible = 1;
		return $photo->save();
	}

	/**
	 * make many photos visible.
	 * @param  array  $ids array of photo ids
	 * @return boolean
	 */
	public function publishMany(array $ids)
	{
		foreach($ids as $photo_id) {
			$this->publish($photo_id);
		}
		return true;
	}

	/**
	 * unpublish the specified photo
	 * @param  string $id
	 * @return boolean
	 */
	public function unpublish($id)
	{
		$photo = $this->Photo->findOrFail($id);

		$photo->visible = 0;
		return $photo->save();
	}

	/**
	 * unpublish (hide) many photos.
	 * @param  array  $ids array of photo ids
	 * @return boolean
	 */
	public function unpublishMany(array $ids)
	{
		foreach($ids as $photo_id) {
			$this->unpublish($photo_id);
		}
		return true;
	}

	/**
	 * unpublish all photos except those listed
	 * @param  array  $ids
	 * @return boolean
	 */
	public function unpublishExcept(array $ids)
	{
		$photos = $this->except($ids);

		foreach($photos as $photo) {
			$this->unpublish($photo->id);
		}
		return true;
	}

	/**
	 * mark the desired photo as a contest winner
	 * @param  string $id
	 * @return boolean
	 */
	public function markAsWinner($id)
	{
		$photo = $this->Photo->findOrFail($id);

		$photo->has_won = 1;
		return $photo->save();
	}

	/**
	 * mark many photos as winners
	 * @param  array  $ids array of photo ids
	 * @return boolean
	 */
	public function markManyAsWinners(array $ids)
	{
		foreach($ids as $photo_id) {
			$this->markAsWinner($photo_id);
		}
		return true;
	}

	/**
	 * removes the winning flag to the specified photo
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function unmarkAsWinner( $id )
	{
		$photo = $this->Photo->findOrFail($id);

		$photo->has_won = 0;
		return $photo->save();
	}

	/**
	 * removes the winning flag to many photos specified by their id
	 *
	 * @param array $ids
	 *
	 * @return mixed
	 */
	public function unmarkManyAsWinners( array $ids )
	{
		foreach($ids as $photo_id) {
			$this->unmarkAsWinner($photo_id);
		}
		return true;
	}

	/**
	 * unmarks all photos the winning flag except those specified
	 *
	 * @param array $ids
	 *
	 * @return mixed
	 */
	public function unmarkWinnersExcept( array $ids )
	{
		$photos = $this->except($ids)->lists('id');
		return $this->unmarkManyAsWinners($photos);
	}
}
