<?php namespace Listerine\Repositories\Contracts;

interface PhotosProvider {

	/**
	 * return all the photos in database
	 * @return Collection
	 */
	public function all();

	/**
	 * return all the photos ordered by creation date (from earliest to oldest)
	 * @return Collection
	 */
	public function recent();

	/**
	 * creates a new media or retrieves the media object
	 * if it was already in database
	 *
	 * @param $mediaInfo
	 * @param $owner
	 *
	 * @return Photo
	 */
	public function firstOrCreate($mediaInfo, $owner);

	/**
	 * return all winner photos organized by date
	 * @return Collection
	 */
	public function winners();

	/**
	 * return all the photos that have been shared today
	 * @return Collection
	 */
	public function publishedToday();

	/**
	 * return only visible photos publiushed between the date range
	 *
	 * @param  Carbon $date1
	 * @param  Carbon $date2
	 * @return Collection
	 */
	public function publishedBetween($date1, $date2);

	/**
	 * return only the specified photos
	 * @param  array $ids
	 * @return Collection
	 */
	public function only(array $ids);

	/**
	 * get all photos except those listed
	 * @param  array  $ids
	 * @return Collection
	 */
	public function except(array $ids);

	/**
	 * return only the visible images
	 * @return Collection
	 */
	public function visible();

	/**
	 * updates a single photo
	 * @param  string $photo_id the photo id
	 * @param  array $fields    an array with the updated fields
	 * @return boolean
	 */
	public function update($photo_id, array $fields);

	/**
	 * updates many photos at once
	 * @param  array  $photos photos array with field info
	 * @return boolean
	 */
	public function updateMany(array $photos);

	/**
	 * make the desired photo visible
	 * @param  string $id
	 * @return boolean
	 */
	public function publish($id);

	/**
	 * make many photos visible.
	 * @param  array  $ids array of photo ids
	 * @return boolean
	 */
	public function publishMany(array $ids);

	/**
	 * unpublish the specified photo
	 * @param  string $id
	 * @return boolean
	 */
	public function unpublish($id);

	/**
	 * unpublish (hide) many photos.
	 * @param  array  $ids array of photo ids
	 * @return boolean
	 */
	public function unpublishMany(array $ids);

	/**
	 * unpublish all photos except those listed
	 * @param  array  $ids
	 * @return boolean
	 */
	public function unpublishExcept(array $ids);

	/**
	 * mark the desired photo as a contest winner
	 * @param  string $id
	 * @return boolean
	 */
	public function markAsWinner($id);

	/**
	 * mark many photos as winners
	 * @param  array  $ids array of photo ids
	 * @return boolean
	 */
	public function markManyAsWinners(array $ids);

	/**
	 * removes the winning flag to the specified photo
	 * @param $id
	 *
	 * @return mixed
	 */
	public function unmarkAsWinner($id);

	/**
	 * removes the winning flag to many photos specified by their id
	 *
	 * @param array $ids
	 *
	 * @return mixed
	 */
	public function unmarkManyAsWinners(array $ids);

	/**
	 * unmarks all photos the winning flag except those specified
	 * @param array $ids
	 *
	 * @return mixed
	 */
	public function unmarkWinnersExcept(array $ids);
}