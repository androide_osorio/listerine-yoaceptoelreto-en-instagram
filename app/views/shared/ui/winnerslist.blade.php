<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

	@foreach($all_winners as $date => $winners)
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingOne">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				<h4 class="panel-title">
					{{ $date }}
				</h4>
			</a>
		</div>
		<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
			<div class="panel-body">
				@foreach($winners as $winner)
					<div class="foto">
						<a href="{{ $winner->instagram_url }}" target="_new">
							<img src="{{ $winner->image_url }}" alt="" />
							<span style="color:#00bec9;">@</span>{{ $winner->username }}
						</a>
					</div>
				@endforeach
			</div>
		</div>
	</div>
	@endforeach
</div>