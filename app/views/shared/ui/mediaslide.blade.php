<div class="participantes" style="padding-top:12px;">
	<div class="slider">
		@foreach($media as $photo)
		<div class="foto" id="{{ $photo->id }}">
			<img src="{{ $photo->image_url }}" alt="{{ $photo->username }}" />
			<h4><span style="color:#23b9c3;">@</span>{{ $photo->username }}</h4>
		</div>
		@endforeach
	</div>

	<div id="carousel-example-generic" class="carousel slide mob-slide" data-ride="carousel" style="display:none;">

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			@foreach($media as $photo)
			<div class="item">
				<img src="{{ $photo->image_url }}" alt="{{ $photo->username }}" />
				<div class="carousel-caption">
					<span style="color:#23b9c3;">@</span>{{ $photo->username }}
				</div>
			</div>
			@endforeach
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-izquierdo" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-derecho" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>


	<h2 id="porque" style="font-size: 13px; text-decoration: underline; float: right; margin-top: -18px; margin-right: 21px;font-family: 'gotham_bookregular';">
		<a href="{{ route('instagram.user.authorize') }}" title="#YoAceptoElReto - Autoriza a LISTERINE® a ver tus fotos para participar" id="instagram-authorize">¿Por qué mi foto no aparece?</a>
	</h2>
</div>