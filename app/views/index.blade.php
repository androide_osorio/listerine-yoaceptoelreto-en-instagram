@extends('layouts.master')

@section('content')
	{{-- Header --}}
	<section style="margin-top: 10px; margin-top: 10px; padding-bottom: 20px; padding-top: 20px;">
		<div class="column" style="width:20%;"><img src="{{ asset('public/img/logo-reto21dias.svg') }}" width="80px" alt="" /></div>
		<div class="column" style="width:60%; margin-left: 12px;">
		<h1>¿Cómo ganar tu LISTERINE<sup style="font-size: 9px; vertical-align: 13px;">®</sup> para el Reto 21 días?</h1></div>
		<div class="about">
			<h1>Todos los días premiaremos las 2 mejores fotos de Instagram con una botella de LISTERINE®</h1>
		</div>
	</section>
	{{-- / Header --}}


	@if($media->count() !== 0)
		<section id="participantes">
			<h1 style="margin-top:17px;">HOY PARTICIPAN <b style="background-color: #035A60; padding: 5px; font-size: 27px;">{{ $media->count() }}</b> RETADORES</h1>
			<h1 style="display: inline-block; vertical-align: top; margin-top: 28px; font-size: 25px; margin-left: 14px;">CON</h1><img style="display: inline-block; margin-top: 15px;" src="{{ asset('public/img/hashtag.png') }}" alt="" />
		</section>

		<section style="padding-bottom: 20px;">
			@include('shared.ui.mediaslide')
		</section>
	@else
		<section id="participantes">
				<h1 style="margin-top:17px;">NADIE HA ACEPTADO EL RETO HOY</h1>
				<h1 style="display: inline-block; vertical-align: top; margin-top: 28px; font-size: 25px; margin-left: 14px;">SE EL PRIMERO EN DECIR </h1><img style="display: inline-block; margin-top: 15px;" src="{{ asset('public/img/hashtag.png') }}" alt="" />
				<h2 style="font-size: 13px; text-decoration: underline; float: right; margin-top: -18px; margin-right: 21px;font-family: 'gotham_bookregular';">
					<a href="{{ route('instagram.user.authorize') }}" title="#YoAceptoElReto - Autoriza a LISTERINE® a ver tus fotos para participar" id="instagram-authorize">¿Por qué mi foto no aparece?</a>
				</h2>
		</section>
	@endif

	@unless($all_winners->isEmpty())
	<section style="margin-top: 15px; margin-bottom:35px;">
		<div class="title">
			<h1>GANADORES</h1>
		</div>
		<div class="contenido-ganadores" style="padding:20px;">
			@include('shared.ui.winnerslist', array('all_winners' => $all_winners))
		</div>
	</section>
	@endunless
@stop

@section('body-scripts')
  {{ HTML::script('public/js/instafeed.min.js') }}
  {{ HTML::script('public/js/main.js') }}

@stop