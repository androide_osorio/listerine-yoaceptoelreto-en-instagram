<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Administrador - #YoAceptoElReto - Listerine® Reto 21 Días</title>

	{{-- Stylesheets --}}
	{{ HTML::style('css/styles.css') }}
	{{ HTML::style('css/bootstrap.min.css') }}
	{{ HTML::style('css/slick.css') }}
	{{ HTML::style('css/slick-theme.css') }}

	{{-- scripts --}}
	{{ HTML::script('js/jquery.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}
	{{ HTML::script('js/slick.min.js') }}
</head>

<body class="admin">
	<div class="reto-instagram">
		@yield('content')
	</div>

	{{-- Bottom scripts --}}
	@yield('body-scripts')
</body>
</html>