<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>#YoAceptoElReto - Listerine® Reto 21 Días</title>

	{{-- Stylesheets --}}
	{{ HTML::style('public/css/styles.css') }}
	{{ HTML::style('public/css/bootstrap.min.css') }}
	{{ HTML::style('public/css/slick.css') }}
	{{ HTML::style('public/css/slick-theme.css') }}

	{{-- scripts --}}
	{{ HTML::script('public/js/jquery.js') }}
	{{ HTML::script('public/js/bootstrap.min.js') }}
	{{ HTML::script('public/js/slick.min.js') }}
	<script type="text/javascript" src="//w.sharethis.com/button/buttons.js"></script>
</head>

<body>
	@include('shared.socialmedia.facebooksdk')

	<div class="reto-instagram">
		@include('shared.navigation.navbar', array('active' => ''))

		@yield('content')

		@include('shared.socialmedia.sharethis')
	</div>

		{{-- Bottom scripts --}}
		<script type="text/javascript">_satellite.pageBottom();</script>
		@yield('body-scripts')
	</body>
	</html>
