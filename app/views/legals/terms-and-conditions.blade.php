@extends('layouts.master')

@section('content')
<section style="margin-top: 10px; margin-top: 10px; padding-bottom: 20px; padding-top: 20px;">

	<div class="title" style="padding:3px;">
		<h2>TÉRMINOS Y CONDICIONES</h2>
		<p>*Actividad válida del 08/06/15 al 08/07/15 | Aplican condiciones*</p>
	</div>

			<div class="terminos" style="padding: 20px; font-family: 'gotham_bookregular'; text-align: justify; color: #888;">
				La actividad se llevará a cabo en la cuenta de LISERINE<sup>®</sup> en Instagram <a href="https://instagram.com/listerineco/">@LISTERINECO</a> y su promoción se hará por medio de las cuentas en Facebook <a href="https://www.facebook.com/ListerineCO">https://www.facebook.com/ListerineCO</a> y Twitter <a href="https://www.twitter.com/ListerineCO">@LISTERINECO</a><br><br>

				<h3>Objetivo</h3>
				Generar interacción de las seguidores y consumidores con la marca en sus redes sociales, haciendo uso del HASHTAG <strong>#YoAceptoElReto.</strong>

				<h3>Duración</h3>
				La actividad inicia el 8 de junio hasta el 28 de junio de 2015

				<h3>Mecánica</h3>
				La marca LISTERINE<sup>®</sup> promocionará en su cuenta de Instagram veintiún (21) posts en los que se reta a los consumidores a compartir su foto en el que demuestran que adoptaron el reto y la suban a su cuenta de instagram haciendo uso del hastag #YoAceptoElReto.<br>
				Seleccionaremos 2 ganadores diariamente, se premiarán con productos LISTERINE<sup>®</sup>, de acuerdo al footprint de la marca y compatibilidad con el reto que se les propuso. Johnson & Johnson de Colombia S.A. seleccionará a su propia discreción y en forma subjetiva a las personas ganadoras. Al participar de esta actividad, autorizan el uso ilimitado a Johnson Johnson a nivel mundial del material sin ninguna compensación adicional en dinero o en especie, incluyendo la imagen de la persona. Solo podrán participar mayores de edad.<br>
				Tus fotos participarán en el concurso si, cumples estos criterios:<br><br>

				<ul style="list-style: none; margin: 0; margin-left: 5px; padding: 0;">
					<li>1.	Eres seguidor de @ListerineCO en Instagram Tu(s) foto(s) han sido publicada(s) entre el 08 de Junio, 2015 y el 28 de Junio, 2015.</li>
					<li>2.	Incluíste el hashtag #YoAceptoElReto en la foto o en el comentario.</li>
					<li>3.	2 En los ajustes de privacidad de tu cuenta de Instagram has autorizado el acceso a Acepta nuestro reto en Instagram a tus fotografías. Accediendo a este link: www.listerine.co/de-que-se-trata-el-reto-21-dias
					</li>
				</ul><br>

				La selección de ganadores se realizará el día miércoles 30 de Junio a las 12m.
				Desde nuestra cuenta en facebook (LISTERINECO), nuestra cuenta de instagram y nuestra cuenta de Twitter, haremos comment mencionando a los ganadores y nos comunicaremos via direct message en Instagram pidiéndole todos sus datos personales y los datos para enviar el premio. El premio consiste en:<br><br>

				<span style="color:#23b9c3;">1 BOTELLA 500ML DE CUALQUIERA DE NUESTRAS PRESENTACIONES DE LISTERINE<sup>®</sup></span><br><br>

				Para reclamar el premio las participantes deberán cumplir con:<br><br>

				<ul style="list-style: none; margin: 0; margin-left: 5px; padding: 0;">
					<li>1.	Mayores de edad</li>
					<li>2.	El destino de envío debe ser dentro de territorio Colombiano.</li>
					<li>3.	Los premios serán entregados entre los días 8, 9 o 10 de julio.</li>
					<li>*Solo se entregará el premio en territorio Colombiano. En las siguientes ciudades:</li><br>

				</ul>

				<ul style="margin-top: -10px; margin-left: 10px">
					<li>Bogotá</li>
					<li>
						Medellín
					</li>
					<li>
						Cali
					</li>
					<li>
						Barranquilla
					</li>
					<li>
						Bucaramanga
					</li>
					<li>
						Cartagena
					</li>
					<li>
						Pereira
					</li>
					<li>
						Manizales
					</li>
					<li>
						Ibagué
					</li>
					<li>
						Armenia
					</li>
					<li>
						Agustin Codazzi - Cesar
					</li>
					<li>
						Albán - Cundinamarca
					</li>
					<li>
						Amaime
					</li>
					<li>
						Andalucia-Valle
					</li>
					<li>
						Arbelaez - Cundinamarca
					</li>
					<li>
						Arjona - Bolivar
					</li>
					<li>
						Baranoa - Atlantico
					</li>
					<li>
						Barrancabermeja
					</li>
					<li>
						Bayunca - Bolivar
					</li>
					<li>
						Bello - Antioquia
					</li>
					<li>
						Bojacá - Cundinamarca
					</li>
					<li>
						Briceño
					</li>
					<li>
						Buenaventura
					</li>
					<li>
						Buga
					</li>
					<li>
						Bugalagrande
					</li>
					<li>
						Cachipay - Cundinamarca
					</li>
					<li>
						Caicedonia - Valle
					</li>
					<li>
						Cajicá
					</li>
					<li>
						Calarca
					</li>
					<li>
						Caldas - Antioquia
					</li>
					<li>
						Candelaria - Valle
					</li>
					<li>
						Cartago
					</li>
					<li>
						Cerete
					</li>
					<li>
						Cerrito - Valle
					</li>
					<li>
						Cerritos - Risarlada
					</li>
					<li>
						Chía
					</li>
					<li>
						Chinacota - Norte de Santander
					</li>
					<li>
						Chinauta
					</li>
					<li>
						Chinchina - Caldas
					</li>
					<li>
						Circasia - Quindio
					</li>
					<li>
						Clemencia - Bolivar
					</li>
					<li>
						Copacabana - Antioquia
					</li>
					<li>
						Cota - Cundinamarca
					</li>
					<li>
						Cuba
					</li>
					<li>
						Cúcuta
					</li>
					<li>
						Cumaca - Cundinamarca
					</li>
					<li>
						Dosquebradas
					</li>
					<li>
						Duitama
					</li>
					<li>
						El Cerrito - Valle
					</li>
					<li>
						El Peñón - Cundinamarca
					</li>
					<li>
						El Rosal - Cundinamarca
					</li>
					<li>
						El Santuario - Antioquia
					</li>
					<li>
						Envigado
					</li>
					<li>
						Espinal
					</li>
					<li>
						Facatativá
					</li>
					<li>
						Filandia
					</li>
					<li>
						Flandes - Girardot
					</li>
					<li>
						Florida - Valle
					</li>
					<li>
						Floridablanca - Santander
					</li>
					<li>
						Funza - Cundinamarca
					</li>
					<li>
						Fusagasugá
					</li>
					<li>
						Galapa - Atlantico
					</li>
					<li>
						Garzon - Huila
					</li>
					<li>
						Genova
					</li>
					<li>
						Girardot
					</li>
					<li>
						Girardota - Antioquia
					</li>
					<li>
						Giron - Santander
					</li>
					<li>
						Granada - Cundinamarca
					</li>
					<li>
						Guayabal - Antioquia
					</li>
					<li>
						Guayabal de Síquima - Cundinam
					</li>
					<li>
						Itagüí
					</li>
					<li>
						Jamundi - Valle
					</li>
					<li>
						La Calera - Cundinamarca
					</li>
					<li>
						La Ceja - Antioquia
					</li>
					<li>
						La Estrella - Antioquia
					</li>
					<li>
						La Jagua de Ibirco
					</li>
					<li>
						La Virginia - Risaralda
					</li>
					<li>
						Madrid - Cundinamarca
					</li>
					<li>
						Malambo - Atlantico
					</li>
					<li>
						Melgar
					</li>
					<li>
						Montelibano
					</li>
					<li>
						Montenegro
					</li>
					<li>
						Montenegro - Quindio
					</li>
					<li>
						Montería
					</li>
					<li>
						Mosquera - Cundinamarca
					</li>
					<li>
						Neira - Caldas
					</li>
					<li>
						Neiva
					</li>
					<li>
						Ocaña - Norte de Santander
					</li>
					<li>
						Palmira
					</li>
					<li>
						Pamplona - Norte de Santander
					</li>
					<li>
						Pasca - Cundinamarca
					</li>
					<li>
						Pasto
					</li>
					<li>
						Piedecuesta-Santander
					</li>
					<li>
						Pijao
					</li>
					<li>
						Pitalito - Huila
					</li>
					<li>
						Planeta Rica
					</li>
					<li>
						Popayán
					</li>
					<li>
						Pradera - Valle
					</li>
					<li>
						Pueblo Tapao
					</li>
					<li>
						Puerto Colombia - Atlantico
					</li>
					<li>
						Quimbaya- Quindio
					</li>
					<li>
						Rancheria
					</li>
					<li>
						Ricaurte
					</li>
					<li>
						Rio Negro - Antioquia
					</li>
					<li>
						Riohacha
					</li>
					<li>
						Rodadero - Magdalena
					</li>
					<li>
						Roldanillo
					</li>
					<li>
						Sabanalarga - Atlantico
					</li>
					<li>
						Sabaneta - Antioquia
					</li>
					<li>
						Salento
					</li>
					<li>
						San Agustin - Huila
					</li>
					<li>
						San Antonio de Pereira - Antio
					</li>
					<li>
						San Antonio de Prado - Antioqu
					</li>
					<li>
						San Cristobal - Antioquia
					</li>
					<li>
						San Gil
					</li>
					<li>
						San Gil - Santander
					</li>
					<li>
						San Juan del Cesar
					</li>
					<li>
						San Pedro - Tulua
					</li>
					<li>
						Santa Marta
					</li>
					<li>
						Santa Rosa de Cabal
					</li>
					<li>
						Santo Tomas - Atlantico
					</li>
					<li>
						Sevilla - Valle
					</li>
					<li>
						Sibate - Cundinamarca
					</li>
					<li>
						Siberia
					</li>
					<li>
						Silvania - Cundinamarca
					</li>
					<li>
						Sincelejo
					</li>
					<li>
						Sinu
					</li>
					<li>
						Soacha
					</li>
					<li>
						Socorro - Santander
					</li>
					<li>
						Sogamoso
					</li>
					<li>
						Soledad- Atlantico
					</li>
					<li>
						Sopó
					</li>
					<li>
						Subachoque - Cundinamarca
					</li>
					<li>
						Tabio - Cundinamarca
					</li>
					<li>
						Tebaida
					</li>
					<li>
						Tenjo
					</li>
					<li>
						Tenjo - Cundinamarca
					</li>
					<li>
						Tibacuy
					</li>
					<li>
						Timana - Huila
					</li>
					<li>
						Tocaima - Cundimarca
					</li>
					<li>
						Tocancipa
					</li>
					<li>
						Trujillo- Valle
					</li>
					<li>
						Tuluá
					</li>
					<li>
						Tunja
					</li>
					<li>
						Ureña - Cucuta
					</li>
					<li>
						Valledupar
					</li>
					<li>
						Villamaria - Caldas
					</li>
					<li>
						Villavicencio
					</li>
					<li>
						Yumbo
					</li>
					<li>
						Zapatoca - Santander
					</li>
					<li>
						Zarsal - Valle
					</li>
					<li>
						Zipacón - Cundinamarca
					</li>
					<li>
						Zipaquirá
					</li>
				</ul>




			</div>

</section>
@stop