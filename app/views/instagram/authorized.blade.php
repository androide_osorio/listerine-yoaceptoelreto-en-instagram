@extends('layouts.master')

@section('content')
<section style="margin-top: 10px; margin-top: 10px; padding-bottom: 20px; padding-top: 20px;">

	<div class="title" style="padding:10px;">
		<h2>GRACIAS POR AUTORIZAR ACCESO A TUS FOTOS</h2>
	</div>

	<ul class="pasos">
		<li>
			<div class="numero">
				1
			</div>

			<div class="descripcion" style="width: 79%; font-size: 15px;">
				<p>Esta ventana se cerrará automáticamente en unos segundos. Recarga la página para ver tus fotos.</p>
			</div>
		</li>
	</ul>

</section>
@stop

@section('body-scripts')
	<script>
	setTimeout(function(){
		window.close();
	}, 9000);
	</script>
@stop