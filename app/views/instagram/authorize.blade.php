@extends('layouts.master')

@section('content')
<section style="margin-top: 10px; margin-top: 10px; padding-bottom: 20px; padding-top: 20px;">

	<div class="title" style="padding:10px;">
		<h2>TU FOTO NO APARECE PORQUE</h2>
	</div>

	<ul class="pasos">
		<li>
			<div class="numero">
				1
			</div>

			<div class="descripcion" style="width: 79%; font-size: 15px;">
				<p>En los ajustes de privacidad de tu cuenta has limitado el acceso a tus fotografías. ¿Quieres autorizar el acceso de LISTERINE® a tus fotografías?</p>
			</div>
		</li>
	</ul>

	<ul class="botones">
		<li>
			<a href="{{ route('instagram.login') }}" class="boton">Si, quiero autorizar</a>
			<p>*Debes iniciar sesión en Instagram</p>
		</li>
		<li>
			<a href="Javascript:window.close()" class="boton" data-action="close-window">No, no quiero autorizar</a>
			<p>*No podrás participar en el concurso</p>
		</li>
	</ul>


</section>
@stop