@extends('layouts.admin')

@section('content')
<h1>Administrador #YoAceptoElReto</h1>
<div class="row">
	<div class="col-md-8">
		<form action="" method="post" accept-charset="utf-8">
			@if($media->count() !== 0)
				@include('admin.winners_form')
			@else
				<section id="participantes">
						<h1 style="margin-top:17px;">NADIE HA ACEPTADO EL RETO HOY</h1>
						<h1 style="display: inline-block; vertical-align: top; margin-top: 28px; font-size: 25px; margin-left: 14px;">SE EL PRIMERO EN DECIR </h1><img style="display: inline-block; margin-top: 15px;" src="img/hashtag.png" alt="" />
				</section>
			@endif
		</form>

	</div>

	<div class="col-md-4">
		@unless($all_winners->isEmpty())
		<section style="margin-top: 15px; margin-bottom:35px;">
			<div class="title">
				<h1>GANADORES</h1>
			</div>
			<div class="contenido-ganadores" style="padding:20px;">
				@include('shared.ui.winnerslist', array('all_winners' => $all_winners))
			</div>
		</section>
		@endunless
	</div>
</div>
@stop
