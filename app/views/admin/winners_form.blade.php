<section id="participantes" style="  padding-bottom: 22px;">
	<h1 style="margin-top:17px;">ESTÁN PARTICIPANDO <b style="background-color: #035A60; padding: 5px; font-size: 27px;">{{ $media->count() }}</b> RETADORES</h1>
</section>

<section style="padding-bottom: 20px;">
	@foreach($media as $photo)

	<div class="foto foto-admin @if($photo->publishedToday())foto-new@endif @if($photo->has_won)foto-winner@endif" id="{{ $photo->id }}">
		<a href="{{ $photo->instagram_url }}">
			@if($photo->isMissing())
				<img src="{{ asset('public/img/missing-photo.jpg') }}" alt="404 this photo has been deleted" />
			@else
				<img src="{{ $photo->image_url }}" alt="{{ $photo->username }}" />

			@endif
		</a>
		<h5>
			{{ '@'.$photo->username }}
			@if(in_array($photo->username, $winner_users))
				<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
			@endif
		</h5>
		@if($photo->isMissing())
			The user deleted this photo.
		@else
			<input type="checkbox" name="photos[{{$photo->id}}][visible]" value="1" id="photo-{{ $photo->id }}-input" @if($photo->visible) checked @endif>visible?
			<input type="checkbox" name="photos[{{$photo->id}}][has_won]" value="1" id="photo-{{ $photo->id }}-input" @if($photo->has_won) checked @endif>ganador?
			<label for="has-won-at-input">fecha en que ganó:</label>
			<input type="date" name="photos[{{$photo->id}}][has_won_at]" value="{{ is_null($photo->has_won_at) ? '' : $photo->has_won_at->format('Y-m-d') }}" id="has-won-at-input">
		@endif
		<hr>
		<span><strong>published:</strong> {{ $photo->created_at->diffForHumans() }}</span>
	</div>
	@endforeach

	<div class="clear"></div>
	<hr>
	<input type="submit" value="ACEPTAR" class="btn btn-primary">

</section>
