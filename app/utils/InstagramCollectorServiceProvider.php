<?php namespace Listerine\Instagram;

use \Instagram\Instagram;
use Listerine\Instagram\InstagramCollector;
use Illuminate\Support\ServiceProvider;

class InstagramCollectorServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;


	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('Listerine\Instagram\Contracts\InstagramClient', function($app)
		{
			$access_token = $app->config['instagram::default_access_token'];
			$hashtags     = $app->config['instagram::hashtags'];
			$instagram    = new Instagram($access_token);

			return new InstagramCollector($instagram, $hashtags);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('instagram-collector');
	}

}