<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Main Page Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/', array(
	'as' => 'home',
	'uses' => 'HomeController@index'
));

Route::get('como-participo', array(
	'as' => 'pages.how-to-participate',
	'uses' => 'PagesController@renderHowTo'
));

Route::get('terminos-y-condiciones', array(
	'as' => 'legals.terms-and-conditions',
	'uses' => 'PagesController@renderTerms'
));

/*
|--------------------------------------------------------------------------
| Instagram authorization routes
|--------------------------------------------------------------------------
*/
Route::group(array('prefix' => 'instagram'), function()
{
	//callback URI for instagram authentication
	Route::get('/authorized', array(
		'as' => 'instagram.authorize',
		'uses' => 'InstagramController@getAuthorize'
	));

	//returns instagram login url
	Route::get('/login', array(
		'as' => 'instagram.login',
		'uses' => 'InstagramController@getLogin'
	));

	// logs user out of instagram AND the app
	Route::get('/logout', array(
		'as' => 'instagram.logout',
		'uses' => 'InstagramController@getLogout'
	));

	//renders authorization message window
	Route::get('/authorize', array(
		'as'   => 'instagram.user.authorize',
		'uses' => 'InstagramController@renderAuthorize'
	));

});

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/
Route::group(array('prefix' => 'admin'), function()
{
	//renders the admin index page
	Route::get('/', array(
		'as' => 'admin.index',
		'uses' => 'AdminController@index'
	));

	//endpoint for submitting winners to the database
	Route::post('/', array(
		'as' => 'admin.store',
		'uses' => 'AdminController@store'
	));

	Route::get('/monitorize', array(
		'uses' => 'AdminController@monitorize'
	));
});
