(function($, window, document) {
	'use strict';

	var authorizationWindow;

	//-----------------------------------
	function popitup(url, name, width, height) {
		var dimensions = 'width=' + width + ',height=' + height;
		var newWindow = window.open(url, name, dimensions);

		if (window.focus) {
			newWindow.focus();
		}

		authorizationWindow = newWindow

		return false;
	};

	function closeWindow() {
		authorizationWindow.close();
	}

	//-----------------------------------

	//document.ready
	$(function() {
		$('.slider').slick({
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 3
		});

		$('a#instagram-authorize').click(function(event) {
			event.preventDefault();

			var $this = $(this);
			popitup($this.attr('href'), $this.attr('title'), 700, 500);
		});

		$('a[data-action="close-window"]').click(function(event) {
			event.preventDefault();
			window.close();
		})
	});
})(window.jQuery, window, document);